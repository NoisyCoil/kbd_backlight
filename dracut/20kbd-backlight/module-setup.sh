#!/usr/bin/bash

check() {
    local _install _mod
    for _mod in /usr/lib/modules/"$kernel"/kernel/drivers/leds/leds-pwm.ko*; do
        if [ -e "$_mod" ]; then
            _install=Y
        fi
    done
    [ "$_install" == "Y" ] || return 1
    return 0
}

installkernel() {
    instmods leds-pwm
}

install() {
    inst_hook cmdline 20 "$moddir/parse-kbd_backlight.sh"
    inst_simple "$moddir/kbd_backlight" "/sbin/kbd_backlight"
}
