#!/usr/bin/sh

command -v getarg >/dev/null || . /lib/dracut-lib.sh
KBD_BACKLIGHT_BRIGHTNESS="$(getarg rd.kbd_backlight= 2>/dev/null)"
[ "$KBD_BACKLIGHT_BRIGHTNESS" != "" ] && /sbin/initqueue --settled --unique --onetime /sbin/kbd_backlight "$KBD_BACKLIGHT_BRIGHTNESS"
unset KBD_BACKLIGHT_BRIGHTNESS
