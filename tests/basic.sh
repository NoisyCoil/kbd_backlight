#!/bin/sh

CMD="src/kbd_backlight"

SYS_KBD_BACKLIGHT="/sys/class/leds/kbd_backlight/brightness"

if ! [ -f "$SYS_KBD_BACKLIGHT" ]; then
    echo "Error: module not loaded" >&2
    exit 2
fi

KBL0="$(cat $SYS_KBD_BACKLIGHT)"

echo "Running test \`kbd_backlight\`..."
echo 128 >"$SYS_KBD_BACKLIGHT"
KBL=$($CMD 2>/dev/null)
if [ "$KBL" -ne 128 ]; then
    echo "Test \`kbd_backlight\` failed" >&2
    exit 1
fi

sleep 1

if [ -f "/sys/class/leds/kbd_backlight/max_brightness" ]; then
    echo "Running test \`kbd_backlight --max\`..."
    KBL=$($CMD --max 2>/dev/null)
    if [ "$KBL" -ne "$(cat "/sys/class/leds/kbd_backlight/max_brightness" 2>/dev/null)" ]; then
        echo "Test \`kbd_backlight/sys/class/leds/kbd_backlight/max_brightness MAX\` failed" >&2
        exit 1
    fi

    sleep 1
fi

echo "Running test \`kbd_backlight 128\`..."
$CMD 128
KBL=$($CMD 2>/dev/null)
if [ "$KBL" -ne 128 ]; then
    echo "Test \`kbd_backlight 128\` failed" >&2
    exit 1
fi

sleep 1

echo "Running test \`kbd_backlight +50\`..."
$CMD 128
sleep 1
$CMD +50
KBL=$($CMD 2>/dev/null)
if [ "$KBL" -ne 178 ]; then
    echo "Test \`kbd_backlight +50\` failed" >&2
    exit 1
fi

sleep 1

echo "Running test \`kbd_backlight -50\`..."
$CMD 128
sleep 1
$CMD -50
KBL=$($CMD 2>/dev/null)
if [ "$KBL" -ne 78 ]; then
    echo "Test \`kbd_backlight -50\` failed" >&2
    exit 1
fi

sleep 1

echo "Running test \`kbd_backlight +50%\`..."
$CMD 128
sleep 1
$CMD +50%
KBL=$($CMD 2>/dev/null)
if [ "$KBL" -ne 192 ]; then
    echo "Test \`kbd_backlight +50%\` failed" >&2
    exit 1
fi

sleep 1

echo "Running test \`kbd_backlight -50%\`..."
$CMD 128
sleep 1
$CMD -50%
KBL=$($CMD 2>/dev/null)
if [ "$KBL" -ne 64 ]; then
    echo "Test \`kbd_backlight -50%\` failed" >&2
    exit 1
fi

sleep 1

echo "Running test \`kbd_backlight 50%\`..."
$CMD 128
sleep 1
$CMD 50%
KBL=$($CMD 2>/dev/null)
if [ "$KBL" -ne 64 ]; then
    echo "Test \`kbd_backlight 50%\` failed" >&2
    exit 1
fi

echo "Running test \`kbd_backlight 50%MAX\`..."
$CMD 128
sleep 1
$CMD 50%MAX
KBL=$($CMD 2>/dev/null)
if [ "$KBL" -ne 127 ]; then
    echo "Test \`kbd_backlight 50%MAX\` failed" >&2
    exit 1
fi

sleep 1

echo "Running test \`kbd_backlight MAX\`..."
$CMD 128
sleep 1
$CMD MAX
KBL=$($CMD 2>/dev/null)
if [ "$KBL" -ne 255 ]; then
    echo "Test \`kbd_backlight MAX\` failed" >&2
    exit 1
fi

echo "Done. Resetting brightness..."
echo "$KBL0" >"$SYS_KBD_BACKLIGHT"
