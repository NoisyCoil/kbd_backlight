# kbd_backlight

This repo contains a script and a [dracut](https://github.com/dracutdevs/dracut) module that allow you to control the keyboard's backlight brightness from the command line and during boot (initramfs phase). Both will only work provided that your keyboard's brightness can be controlled via the `/sys/class/leds/kbd_backlight` kernel interface.

## Command-line

The `kbd_backlight` command-line script is part of the `kbd_backlight` package. For its usage, see the [manpage](docs/kbd_backlight.8.md).

## Dracut module

To include the dracut module in your initramfs, you must first install the `dracut-kbd-backlight` package and then recreate the initramfs using dracut,

```sh
sudo dracut -fv
```

This is only required after the first install: the dracut module is automatically included in the initramfs every time the system recreates it.

Once the dracut module is included in your initramfs, you will be able to set the boot-time keyboard's backlight brightness by adding the `rd.kbd_backlight` parameter to the kernel command line. For example, setting

```text
GRUB_CMDLINE_LINUX="rd.kbd_backlight=128"
```

in `/etc/default/grub` and regenerating the GRUB configuration via `grub2-mkconfig` will set the brightness to about half its maximum (on systems where the latter is 255).

## Copyright

Copyright (c) 2023 NoisyCoil (<noisycoil@tutanota.com>). License: MIT (<https://mit-license.org>).
