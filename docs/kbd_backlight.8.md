# NAME

kbd_backlight - control the keyboard backlight from the command line

# SYNOPSIS

**kbd_backlight** \[**+**|**-**\]\[*num*\]\[**%**\]\[**MAX**\]

**kbd_backlight** \[ **--max** \]

# DESCRIPTION

If it can be controlled via the kernel interface located at
/sys/class/leds/kbd_backlight/brightness, **kbd_backlight** allows you
to easily change your keyboard's backlight brightness from the command
line by specifying the new brightness either as an absolute number, or
as a number relative to the current or max brightness.

When **kbd_backlight** is called with no arguments and either 1. no
options, or 2. the **--max** option, it returns 1. the current
brightness, or 2. the maximum brightness.

# EXAMPLES

$ kbd_backlight  
Get the current brightness.

$ kbd_backlight --max  
Get the maximum brightness.

$ kbd_backlight 50  
Set the brightness to 50.

$ kbd_backlight +50  
Increase the brightness by 50.

$ kbd_backlight -50  
Decrease the brightness by 50.

$ kbd_backlight +50%  
Increase the brightness by 50%.

$ kbd_backlight -50%  
Decrease the brightness by 50%.

$ kbd_backlight 50%  
Set the brightness to 50% the current value.

$ kbd_backlight 50%MAX  
Set the brightness to 50% the maximum value.

$ kbd_backlight MAX  
Set the brightness to the maximum value.

# REPORTING BUGS

Bug tracker: &lt;https://gitlab.com/NoisyCoil/kbd_backlight&gt;.

# COPYRIGHT

Copyright (c) 2023 NoisyCoil &lt;noisycoil@tutanota.com&gt;. License:
MIT &lt;https://mit-license.org&gt;.
